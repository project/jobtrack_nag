<?php

/**
 * @file
 * Implementation of token module hooks.
 *
 * @ingroup token
 */

/**
 * Implementation of hook_token_values().
 */
function jobtrack_nag_token_values($type, $object = NULL, $options = array()) {
  $values = array();
  global $base_url;
  switch ($type) {
    case 'jobtrack-autoclose':
      if (isset($object) && is_object($object)) {
        $sids = _jobtrack_get_state(JOBTRACK_STATE_CLOSED);
        foreach ($sids as $sid) {
          $default_closed = _jobtrack_state($sid);
          break;
        }
        $ticket = $object;
        $values['current-state'] = _jobtrack_state($ticket->state);
        $values['closed-state'] = _jobtrack_state(variable_get('jobtrack_nag_autoclose_close_state', $default_closed));
        $values['priority'] = _jobtrack_priorities($ticket->priority);
        $values['client'] = _jobtrack_client($ticket->client);
        $values['inactive-time'] = format_interval(time() - $ticket->changed);
        $values['inactive-limit'] = format_interval(variable_get('jobtrack_nag_autoclose_'. _jobtrack_state($ticket->state), 0));
        $account = user_load(array('uid' => $ticket->uid));
        $values['owner-name'] = $account->name;
        $values['owner-email'] = $account->mail;
      }
      break;
    case 'jobtrack-notify':
      if (isset($object) && is_array($object)) {
        $nids = $object;
        $tickets  = "Client:               Ticket title:                   State:     Priority:\n";
        $tickets .= "--------------------------------------------------------------------------\n";
        $first = TRUE;
        foreach ($nids as $nid) {
          if ($first) {
            $first = FALSE;
          }
          else {
            $tickets .= "\n";
          }
          $node = node_load($nid);
          $tickets .= _jobtrack_nag_truncate(_jobtrack_client($node->client), 20, TRUE);
          $tickets .= '  '. _jobtrack_nag_truncate($node->title, 30, TRUE);
          $tickets .= '  '. _jobtrack_nag_truncate(_jobtrack_state($node->state), 9, TRUE);
          $tickets .= '  '. _jobtrack_nag_truncate(_jobtrack_priorities($node->priority), 9, TRUE);
          $tickets .= "\n". url("node/$node->nid", array('absolute' => TRUE)) ."\n";
        }
        $tickets .= "--------------------------------------------------------------------------";
        $values['tickets'] = $tickets;

        $tickets = '';
        $result = db_query("SELECT n.nid FROM {node} n LEFT JOIN {jobtrack_ticket} jt ON n.nid = jt.nid WHERE n.nid IN (%s) ORDER BY jt.priority DESC, jt.state ASC, jt.client ASC", implode(',', $nids));
        $priority = 0;
        while ($nid = db_fetch_object($result)) {
          $node = node_load($nid->nid);
          if ($node->priority != $priority) {
            if ($priority) {
              $tickets .= " --------------------------------------------------------------\n";
            }
            $priority = $node->priority;
            $tickets .= "\nPriority: ". _jobtrack_nag_truncate(_jobtrack_priorities($node->priority), 10) ."\n\n";
            $tickets .= " Client:              Ticket title:                   State:\n";
            $tickets .= " -----------------------------------------------------------\n";
          }
          else if ($priority) {
            $tickets .= "\n";
          }
          $tickets .= ' '. _jobtrack_nag_truncate(_jobtrack_client($node->client), 19, TRUE);
          $tickets .= '  '. _jobtrack_nag_truncate($node->title, 30, TRUE);
          $tickets .= '  '. _jobtrack_nag_truncate(_jobtrack_state($node->state), 9, TRUE);
          $tickets .= "\n ". url("node/$node->nid", array('absolute' => TRUE)) ."\n";
        }
        $tickets .= " -----------------------------------------------------------";
        $values['tickets-per-priority'] = $tickets;

        $tickets = '';
        $result = db_query("SELECT n.nid FROM {node} n LEFT JOIN {jobtrack_ticket} jt ON n.nid = jt.nid WHERE n.nid IN (%s) ORDER BY jt.state ASC, jt.priority DESC, jt.client ASC", implode(',', $nids));
        $state = 0;
        while ($nid = db_fetch_object($result)) {
          $node = node_load($nid->nid);
          if ($node->state != $state) {
            if ($state) {
              $tickets .= " --------------------------------------------------------------\n";
            }
            $state = $node->state;
            $tickets .= "\nState: ". _jobtrack_nag_truncate(_jobtrack_state($node->state), 10) ."\n\n";
            $tickets .= " Client:              Ticket title:                   Priority:\n";
            $tickets .= " --------------------------------------------------------------\n";
          }
          else if ($state) {
            $tickets .= "\n";
          }
          $tickets .= ' '. _jobtrack_nag_truncate(_jobtrack_client($node->client), 19, TRUE);
          $tickets .= '  '. _jobtrack_nag_truncate($node->title, 30, TRUE);
          $tickets .= '  '. _jobtrack_nag_truncate(_jobtrack_priorities($node->priority), 9, TRUE);
          $tickets .= "\n ". url("node/$node->nid", array('absolute' => TRUE)) ."\n";
        }
        $tickets .= " -----------------------------------------------------------";
        $values['tickets-per-state'] = $tickets;

        $tickets = '';
        $result = db_query("SELECT n.nid FROM {node} n LEFT JOIN {jobtrack_ticket} jt ON n.nid = jt.nid WHERE n.nid IN (%s) ORDER BY jt.client, jt.state ASC, jt.priority DESC", implode(',', $nids));
        $client = 0;
        while ($nid = db_fetch_object($result)) {
          $node = node_load($nid->nid);
          if ($node->client != $client) {
            if ($client) {
              $tickets .= " --------------------------------------------------------------\n";
            }
            $client = $node->client;
            $tickets .= "\nClient: ". _jobtrack_nag_truncate(_jobtrack_client($node->client), 50) ."\n\n";
            $tickets .= " Ticket title:                             State:     Priority:\n";
            $tickets .= " --------------------------------------------------------------\n";
          }
          else if ($client) {
            $tickets .= "\n";
          }
          $tickets .= ' '. _jobtrack_nag_truncate($node->title, 40, TRUE);
          $tickets .= '  '. _jobtrack_nag_truncate(_jobtrack_state($node->state), 9, TRUE);
          $tickets .= '  '. _jobtrack_nag_truncate(_jobtrack_priorities($node->priority), 9, TRUE);
          $tickets .= "\n ". url("node/$node->nid", array('absolute' => TRUE)) ."\n";
        }
        $tickets .= " --------------------------------------------------------------";
        $values['tickets-per-client'] = $tickets;
/*

   Client:              Ticket title:                   State:     Priority:
   1234567890123456789  123456789012345678901234567890  123456789  123456789
   -------------------------------------------------------------------------

   Client:
   --------------------------------------------------
   12345678901234567890123456789012345678901234567890
   Ticket title:                   State:     Priority:
   123456789012345678901234567890  123456789  123456789
   ----------------------------------------------------

   Priority:
   ----------
   1234567890
   Client:              Ticket title:                   State:
   1234567890123456789  123456789012345678901234567890  123456789
   --------------------------------------------------------------

   State:
   ----------
   1234567890
   Client:              Ticket title:                   Priority:
   1234567890123456789  123456789012345678901234567890  123456789
   --------------------------------------------------------------

*/
      }
      break;
  }
  return $values;
}

function jobtrack_nag_token_list($type = 'all') {
  if ($type == 'jobtrack-autoclose' || $type == 'all') {
    $tokens['jobtrack-autoclose']['current-state'] = t('Current ticket state');
    $tokens['jobtrack-autoclose']['closed-state'] = t('Closed ticket state');
    $tokens['jobtrack-autoclose']['priority'] = t('Ticket priority');
    $tokens['jobtrack-autoclose']['client'] = t('Client the ticket is assigned to');
    $tokens['jobtrack-autoclose']['inactive-time'] = t('How long the ticket has been inactive');
    $tokens['jobtrack-autoclose']['inactive-limit'] = t('Maximum time ticket allowed to be inactive');
    $tokens['jobtrack-autoclose']['owner-name'] = t('Name of ticket owner');
    $tokens['jobtrack-autoclose']['owner-email'] = t('Email of ticket owner');
  }
  if ($type == 'jobtrack-notification' || $type == 'all') {
    $tokens['jobtrack-notification']['tickets'] = t('All matching tickets');
    $tokens['jobtrack-notification']['tickets-per-client'] = t('All matching tickets, divided by client');
    $tokens['jobtrack-notification']['tickets-per-priority'] = t('All matching tickets, divided by priority');
    $tokens['jobtrack-notification']['tickets-per-state'] = t('All matching tickets, divided by state');
  }
  return $tokens;
}
